/*global $ */
/*eslint-env browser*/
//========================Load=====================//

$(document).ready(function () {
    "use strict";
    $(".login").addClass('active');
    $(".side-nav .control-side").click(function () {
        $(this).toggleClass("active");
        $('body').toggleClass("side-bar-icon");
    });
    $(".side-nav .control-side").click(function () {
        if( !$("body").hasClass("side-bar-icon" ) ){
            $(".main-panel").addClass("hide-phone");
        }else{
            $(".main-panel").removeClass("hide-phone");
        }
    });
    
    
    
    
    
    $(".notif-btn,.custom-date,.countCard").click(function () {
        $(this).toggleClass("active");
    });
    
    $(window).click(function() {
        $(".notif-btn, .custom-date,.countCard").removeClass("active");
    });
    
    $('.sub-menu, .notif-btn , .custom-date, .showDate ,.countCard,.countDisplay').click(function(event){
        event.stopPropagation();
    });
    
    
    
    
    
    $('#add-type').keyup(function(){
        
        var type = $("#add-type").val();
        if(!(type == "")){
            $(".added-form .form-group .submit-add").show();
        }else{
            $(".added-form .form-group .submit-add").hide();
        }
    });
});
$(document).ready(function () {
    "use strict";
    function reStyle() {
        if ($(window).width() <= 770) {
            $('body').addClass("side-bar-icon");
        } else {
            $('body').removeClass("side-bar-icon");
        }
    }
    reStyle();
    $(window).resize(function () {
        reStyle();
    });
    
    
   
});

/* Add spen */
$(document).ready(function () {
    "use strict";
    $("#collapseTwo .custom-btn").click(function (){
        swal({
          title: "تم إنشاء القسم بنجاح",
          text: "يمكنك الان البدء في اضافه حساباتك داخل القسم الجديد واضافة اختصاراتك!",
          icon: "success",
          button: "تم!",
        });
    });
    $(".added-form .custom-btn").click(function (){
         swal({
          title: "تم  اضافة التنصيف بنجاح",
          text: "يمكنك الان البدء في اضافه حساباتك داخل القسم الجديد واضافة اختصاراتك!",
          icon: "success",
          button: "تم!",
        });
    });
});

/* Upload image */
$(function () {

  // Viewing Uploaded Picture On Setup Admin Profile
  function livePreviewPicture(picture)
  {
    if (picture.files && picture.files[0]) {
      var picture_reader = new FileReader();
      picture_reader.onload = function(event) {
        $('#uploaded').attr('src', event.target.result);
      };
      picture_reader.readAsDataURL(picture.files[0]);
    }
  }

  $('.profile-photo input').on('change', function () {
    $('#uploaded').fadeIn();
    livePreviewPicture(this);
  });

});



var $currentCard, $nextCard, $prevCard;

var animationEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

$('.next').on('click', function (e) {
  e.preventDefault();
  $currentCard = $(this).parent().parent();
  $nextCard = $currentCard.next();
  
  // Activate next step on progress bar
  $('#bar li').eq($('.progresses').index($nextCard)).addClass('active');
  
  // Hide current card and show the next one
  var inAnimation = 'animated fadeIn';
  var outAnimation = 'animated bounceOutRight';
  
  $currentCard.hide()

  $nextCard
    .show()
    .addClass(inAnimation)
    .one(animationEvents, function () {
      $(this).removeClass(inAnimation);
    });  
    
  

});

$('.prev').on('click', function(e) {
  e.preventDefault();
  $currentCard = $(this).parent().parent();
  $prevCard = $currentCard.prev();
  
  // de-activate current step on progress bar
  $('#bar li').eq($('.progresses').index($currentCard)).removeClass('active');
  
  // Show the previous card and hide the current
  var inAnimation = 'animated fadeIn';
  $currentCard.hide();
  
  $prevCard
    .show()
    .addClass(inAnimation)
    .one(animationEvents, function () {
      $(this).removeClass(inAnimation);
    });
});

 function submitType(e){
     e.preventDefault();
     var typetext = $('#add-type').val();
     if(!(typetext == "")){
        $('.added-form .form-group').append('<div class="added-type"><span onclick="removeInputAdded(event)"><img src="images/icons/add-circle.png" alt="remove"></span><input type="text" class="added-input" value="'+typetext+'" disabled></div>');
         $('#add-type').val('');
     }
};
 function submitSpends(e){
     e.preventDefault();
     var textSpends = $('#name-spend').val();
     if(!(textSpends == "")){
        $('.spends-div').append('<div class="added-type"><span onclick="removeInputAdded(event)"><img src="images/icons/add-circle.png" alt="remove"></span><input type="text" class="added-input" value="'+textSpends+'" disabled></div>');
         $('#name-spend,#hm-spend').val('');
     }
     
};
function removeInputAdded(event){
    var target = $( event.target );
//    target.parent().parent().remove();
    swal({
        title: "هل انت متاكد من الحذف ؟",
        text: "بمجرد حذفها ، لن تكون قادرًا على استرداد هذا  مرة اخري!",
        icon: "warning",
        buttons: true,
        dangerMode: true,

    })
        .then((willDelete) => {
        if (willDelete) {
            swal("حسناً! تم الحذف بنجاح", {
                icon: "success",
            });
        } else {
            swal("حسنا تم الغاء عملية الحذف!");
        }
    });
};
$(document).ready(function () {
    "use strict";
    $("table .delete").click(function(){
        swal({
            title: "هل انت متاكد من الحذف ؟",
            text: "بمجرد حذفها ، لن تكون قادرًا على استرداد هذا  مرة اخري!",
            icon: "warning",
            buttons: true,
            dangerMode: true,

        })
        .then((willDelete) => {
             if (willDelete) {
                 swal("حسناً! تم الحذف بنجاح", {
                     icon: "success",
                 });
             } else {
                 swal("حسنا تم الغاء عملية الحذف!");
             }
         });
    });
});

$(document).ready(function () {
     "use strict";
    $(".countCard").click(function (){
       $(".countDisplay").toggleClass("active");
    });
});
$(document).ready(function () {
    $('#datepicker').datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true,
        minViewMode: 1,
        maxViewMode: 1,
        toggleActive: true,

    });
});
$(document).ready(function () {
    var typed = new Typed('.typed1', {
        stringsElement: '.typed-strings1',
        loop: true,
        typeSpeed: 20,
        backSpeed: 10,
        backDelay: 3000,
        startDelay: 200
    });
    var typed = new Typed('.typed2', {
        stringsElement: '.typed-strings2',
        loop: true,
        typeSpeed: 20,
        backSpeed: 10,
        backDelay: 3000,
        startDelay: 200
    });
});